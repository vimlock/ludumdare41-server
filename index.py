#!/usr/bin/env python3

from flask import Flask
from flask import request
from flask import jsonify

from flask.ext.mysql import MySQL

import base64

app = Flask(__name__)

mysql = MySQL();

app.config['MYSQL_DATABASE_USER'] = ''
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = ''
app.config['MYSQL_DATABASE_HOST'] = ''

mysql.init_app(app)
conn = mysql.connect()

@app.route("/")
def index():
    return "Nothing to see here, move along"

@app.route("/world/<seed>/surface/<surface_id>", methods=['GET', 'POST'])
def surface(seed, surface_id):

    seed = int(seed)
    surface_ids = map(int, surface_id.split(","))

    cursor = conn.cursor()

    if request.method == 'POST':

        surface_id = surface_ids[0]

        pixels = request.form["pixels"];
        pixels = base64.b64decode(pixels)

        # print("pixels were %s" % (pixels,));

        cursor.execute("REPLACE INTO surface (id, seed, pixels) VALUES (%s, %s, %s)",
            (surface_id, seed, pixels)
        );

        conn.commit();

        return jsonify(result="success");
    else:

        placeholders = ', '.join('%s' for _ in surface_ids)
        query = "SELECT id, pixels FROM surface WHERE seed = " + conn.escape_string(str(seed)) + (" AND id IN (%s)" % placeholders)

        # print(query);

        cursor.execute(query, surface_ids)

        surfaces = list()
        for row in cursor:
            surfaces.append({
                "id" : row[0],
                "pixels": base64.b64encode(row[1]),
            });

        return jsonify(surfaces=surfaces)


if __name__ == "__main__":
    app.run(host='0.0.0.0')
