
use ldjam41;

drop table if exists surface;

create table surface (
    id integer primary key NOT NULL,
    seed integer NOT NULL,
    pixels BLOB NOT NULL
);
